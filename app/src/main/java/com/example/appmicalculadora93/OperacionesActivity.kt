package com.example.appmicalculadora93

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario: TextView
    private lateinit var txtNume1 : EditText
    private lateinit var txtNume2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSumar : Button
    private lateinit var btnRestar : Button
    private lateinit var btnMul : Button
    private lateinit var btnDiv : Button

    private lateinit var btnRegresar : Button
    private lateinit var btnLimpiar : Button
    private lateinit var operaciones: Operaciones

    var opcion : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventoClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes(){
        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById( R.id.txtResultado)
        txtNume1 = findViewById(R.id.txtNum1)
        txtNume2 = findViewById( R.id.txtNum2)

        btnRegresar = findViewById(R.id.btnRegresar)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnDiv = findViewById( R.id.btnDividir)
        btnMul = findViewById(R.id.btnMultiplicar)
        btnSumar = findViewById( R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)

        val bundle : Bundle? = intent.extras

        txtUsuario.text = bundle?.getString("usuario")


    }

    public fun Validar():Boolean{
        if (txtNume1.text.toString().contentEquals("")
            || txtNume2.text.toString().contentEquals(""))
        return false
        else return true
    }
    public fun operaciones() :  Float{
        var num1:Float=0f
        var num2:Float=0f
        var res:Float=0f

        if (Validar()){
            num1 =txtNume1.text.toString().toFloat()
            num2 =txtNume2.text.toString().toFloat()
            operaciones =Operaciones(num1,num2)

            when(opcion){
                1->{
                    res=operaciones.sumar()
                }
                2->{
                    res=operaciones.restar()
                }
                3->{
                res=operaciones.Multiplicar()
                }
                4->{
                    res=operaciones.dividir()
                }

            }

        }else
        {
            Toast.makeText(this, "Error Faltaron datos", Toast.LENGTH_SHORT).show()
        }
       return res
    }

    public fun eventoClic(){
        btnSumar.setOnClickListener(View.OnClickListener {
            opcion = 1
            txtResultado.text=operaciones().toString()
        })
        btnRestar.setOnClickListener(View.OnClickListener {
            opcion = 2
            txtResultado.text=operaciones().toString()
        })
        btnMul.setOnClickListener(View.OnClickListener {
            opcion = 3
            txtResultado.text=operaciones().toString()
        })
        btnDiv.setOnClickListener(View.OnClickListener {
            if (this.txtNume2.text.toString().toFloat()==0f)
                txtResultado.text = "No es posible dividir sobre 0"
            else
            opcion = 4
            txtResultado.text=operaciones().toString()
        })
        btnRegresar.setOnClickListener(View.OnClickListener {
          val builder = AlertDialog.Builder(this)
          builder.setTitle("Calculadora")
          builder.setMessage("¿quieres salir?")

            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                this.finish()
            }
            builder.setNegativeButton(android.R.string.no) { dialog, which ->

            }
            builder.show()

            btnLimpiar.setOnClickListener(View.OnClickListener {
            txtNume1.text.clear()
            txtNume2.text.clear()
            txtResultado.setText("")
        })

    })


}
}