package com.example.appmicalculadora93

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtUsuario: EditText
    private lateinit var txtContraseña: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        eventoClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
        btnSalir = findViewById(R.id.btnCerrar)
        btnIngresar = findViewById(R.id.btnIngresar)
    }

    private fun eventoClic() {
        btnIngresar.setOnClickListener {
            val usuario: String = getString(R.string.usuario)
            val contraseña: String = getString(R.string.contraseña)
            val nombre: String = getString(R.string.nombre)

            if (txtUsuario.text.toString().equals(usuario) && txtContraseña.text.toString().equals(contraseña)) {
                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("usuario", nombre)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Faltó información o está incorrecta", Toast.LENGTH_SHORT).show()
            }
        }

        btnSalir.setOnClickListener {
            finish()
        }
    }
}
